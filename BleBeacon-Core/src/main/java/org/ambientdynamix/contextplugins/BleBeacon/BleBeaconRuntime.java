/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.BleBeacon;

import java.util.*;

import com.radiusnetworks.ibeacon.*;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
//import com.radiusnetworks.ibeacon.service.IBeaconData;
//import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;

//import android.content.Intent;
//import android.content.ServiceConnection;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;


public class BleBeaconRuntime extends ContextPluginRuntime {

    private static final int VALID_CONTEXT_DURATION = 60000;
    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private IBeaconManager iBeaconManager;
    private UUID listenerId_BeaconArray;       
    private boolean beaconRequest =false;
    private UUID listenerId_MonitorBeaconInfo;       
    //private ArrayList<BeaconInfo> newBeacons = new ArrayList<BeaconInfo>();
    /**
     * set to true if you want to see debugRuntime messages associated with this library
     */
    private static boolean debugRuntime = true;

    public static void setDebug(boolean debug) {
    	BleBeaconRuntime.debugRuntime = debug;
    }

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings)throws Exception {
    	if (BleBeaconRuntime.debugRuntime) Log.d(TAG, "init .............started");
    	
    	this.setPowerScheme(powerScheme);
        this.context = this.getSecuredContext();
        iBeaconManager = IBeaconManager.getInstanceForApplication(this.context); 
        if(!iBeaconManager.checkAvailability()){
        	Log.w(TAG,"Bluetooth LE not supported by this device");
        	destroy();
    	}        
        /*for (Map.Entry<String,String> entry : this.getSettings().entrySet()) {
        	Log.i(TAG," key = "+entry.getKey()+"value = "+entry.getValue());        	
        } */      	
        if (BleBeaconRuntime.debugRuntime) Log.d(TAG, "init .............completed");  		
    	
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start)
     * context sensing or acting operations.
     */
    @Override
    public void start() { 
    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "start......started");
    	iBeaconManager.bind(this.context,consumer);    	    
    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Getting registered context listeners........");
        Iterator<ContextListenerInformation> listenerInfoIterator = getContextListeners().iterator();
    	while(listenerInfoIterator.hasNext()){
    		if (BleBeaconRuntime.debugRuntime)Log.d(TAG,"listener id :"+listenerInfoIterator.next().getListenerId());
    	}        
    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, " listener ids.....confirmed");
    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, " start.....ended");
    	
    }
    
    private IBeaconConsumer consumer = new BeaconConsumer() {
        @Override
        public void onIBeaconServiceConnect() {
        	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "onIBeaconServiceConnect");
            iBeaconManager.setRangeNotifier(range);
            iBeaconManager.setMonitorNotifier(monitor);                
        }
    };
    
    protected RangeNotifier range = new RangeNotifier() {
    	
        @Override
        public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {            
            notifyIbeaconsDataChanged(iBeacons,region);
        }
    };
    
    protected MonitorNotifier monitor = new MonitorNotifier() {

        @Override
        public void didEnterRegion(Region region) { //matched region , inside =1
        	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "inside region of regionId:  "+ region.getUniqueId() +" Major:  "+region.getMajor() + " Minor:  "+region.getMinor());
        }

        @Override
        public void didExitRegion(Region region) {
        	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "outside region of regionId:  "+ region.getUniqueId() +" Major:  "+region.getMajor() + " Minor:  "+region.getMinor());
        }

        @Override
        public void didDetermineStateForRegion(int state, Region region) { //inside 1,outside 0
            updateState(state,region);            
        }
    };
    
    
	@Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo){    	
		if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "in addcontext listener method.........");		
		if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "context type ="+listenerInfo.getContextType());
		//BEACON ARRAY TYPE
    	if(listenerInfo.getContextType().equalsIgnoreCase(BeaconArray.CONTEXT_TYPE)){
    		this.listenerId_BeaconArray = listenerInfo.getListenerId();    		
    		if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Type: BeaconArray ListenerId: "+listenerInfo.getListenerId());      
	                   
    	}
    	// AUTO_NOTIFiCATION REQUEST
    	else if(listenerInfo.getContextType().equalsIgnoreCase(MonitorBeaconInfo.CONTEXT_TYPE)){
    		 listenerId_MonitorBeaconInfo=listenerInfo.getListenerId();
    		 if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Type: MonitorBeaconInfo ListenerId:"+ listenerInfo.getListenerId());			   
    	}
	else {
			sendContextRequestError(listenerInfo.getListenerId(), "NO_CONTEXT_SUPPORT for "+ listenerInfo.getContextType(), ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
			return false;
		}    	
        return true;
		}			
    
	/*@Override
    public boolean removeContextListener(UUID listenerId) {};
   */ 
	
	private void notifyIbeaconsDataChanged(Collection<IBeacon> beacons,Region region) {           		
        ArrayList<BeaconInfo> newBeacons = new ArrayList<BeaconInfo>();
        Iterator<IBeacon> beaconsIterator = beacons.iterator();
        while (beaconsIterator.hasNext()) {            
            IBeacon newBeacon = beaconsIterator.next();            
            String name=newBeacon.getName();
            int proximity=newBeacon.getProximity();
            double distance=newBeacon.getAccuracy();
            String regionID=region.getUniqueId();
            String proximityUUID=newBeacon.getProximityUuid();
            int major=newBeacon.getMajor();
            int minor=newBeacon.getMinor();
            String macaddress=newBeacon.getBluetoothAddress();
            int broadcastingpower=newBeacon.getTxPower();
            int rssi=newBeacon.getRssi();                
            newBeacons.add(new BeaconInfo(name,macaddress,proximity,distance,regionID,proximityUUID,major,minor,broadcastingpower,rssi));
        }
        if (rangingRequest()){//compare regions        	
        	if (BleBeaconRuntime.debugRuntime)Log.i(TAG, newBeacons.size()+" beacons found for regionId:  "+ region.getUniqueId() +" Major:  "+region.getMajor() + " Minor:  "+region.getMinor()); 
        	sendContextEvent(this.listenerId_BeaconArray, new SecuredContextInfo(new BeaconArray(newBeacons),PrivacyRiskLevel.LOW));        	         	
        }            
    }	
	
	private boolean rangingRequest(){
    	if (beaconRequest){
    		return true;
    		}
    	return false;    	
    }												
	

   private void updateState(int state, Region region){
	   if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Monitor State changed for regionId:  "+ region.getUniqueId() + " Major:  "+region.getMajor() + " Minor:  "+region.getMinor());         
        sendContextEvent(listenerId_MonitorBeaconInfo,new SecuredContextInfo(new MonitorBeaconInfo(state,region.getUniqueId(),region.getProximityUuid(),region.getMajor(),region.getMinor()),PrivacyRiskLevel.LOW), VALID_CONTEXT_DURATION);
   }
   
   /**
	 * Called by the Dynamix Context Manager to stop context sensing or acting
	 * operations; however, any acquired resources should be maintained, since
	 * start may be called again.
	 */
    @Override
    public void stop() {
    	beaconRequest=false;    	
    	while(iBeaconManager.getMonitoredRegions().iterator().hasNext()){    		
    		try {    			
    			iBeaconManager.stopMonitoringBeaconsInRegion(iBeaconManager.getMonitoredRegions().iterator().next());
    			if (BleBeaconRuntime.debugRuntime) Log.i(TAG, "Currently in STOP, ending all exisiting monitors....!");  
    		}        
    		catch (RemoteException e) {
    		}
    	}
    	while(iBeaconManager.getRangedRegions().iterator().hasNext()){    		
    		try {
    			iBeaconManager.stopRangingBeaconsInRegion(iBeaconManager.getRangedRegions().iterator().next());  
    			if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Currently in STOP, ending all exisiting rangers....!");  
    		}        
    		catch (RemoteException e) {
    		}
    	}   	
        iBeaconManager.unBind(consumer);       
        if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Plugin stopped!");        
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources
     * in preparation for garbage collection. Once this method has been called,
     * it may not be re-started and will be reclaimed by garbage collection
     * sometime in the indefinite future.
     */
    @Override
    public void destroy() {                
    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Plugin destroyed!");        
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {		
    	if (BleBeaconRuntime.debugRuntime)Log.w(TAG,"handleContextRequest called, but plugin does'nt support non-configured context requests");
    }

    
    //private UUID requestid;
    @Override
    public void handleConfiguredContextRequest(UUID requestId,String contextType, Bundle config) {
    	if(config.getString("BEACON_OPERATION").equalsIgnoreCase("SET_FOREGROUND_SCAN_TIMES")){
    		iBeaconManager.setForegroundScanPeriod(config.getLong("FOREGROUND_SCAN_PERIOD",1100));
    		iBeaconManager.setForegroundBetweenScanPeriod(config.getLong("FOREGROUND_BETWEEN_SCAN_PERIOD",0));
    		try {
				iBeaconManager.updateScanPeriods();
			} catch (RemoteException e) {				
				e.printStackTrace();
			}  
    		if(iBeaconManager.updateScanPeriod){
    			if (BleBeaconRuntime.debugRuntime)Log.i(TAG,"Update scan times initiated...");
    			//sendContextRequestSuccess(requestId);
    		}
    		else{
    			if (BleBeaconRuntime.debugRuntime)Log.i(TAG,"Update scan times failed to initiate...");
    			//sendContextRequestError(requestId, "Incoor", errorCode);
    		}
    		
    	}
    	else if (contextType.equalsIgnoreCase(BeaconArray.CONTEXT_TYPE)) {
        	if(config.getString("BEACON_OPERATION").equalsIgnoreCase("CURRENT_BEACONS")){
        		if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Request to start ranging current beacons received......");
        		//String ESTIMOTE_BEACON_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
        		Region ALL_ESTIMOTE_BEACONS_REGION = new Region("currentBeacons",null, null, null);
        		try {
        			this.iBeaconManager.startRangingBeaconsInRegion(ALL_ESTIMOTE_BEACONS_REGION);						
        		} 
        		catch (RemoteException e) {
        		} 
        		beaconRequest=true;
        		/*sendContextRequestSuccess(requestId);*/
        		if (BleBeaconRuntime.debugRuntime)Log.i(TAG, " Requested region valid and started !");  
        	}
        	else if(config.getString("BEACON_OPERATION").equalsIgnoreCase("RANGE")){
        			if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Request to start ranging received......");				
        			String beaconRegionId = config.getString("REGIONID");
        			String beaconUUID = config.getString("UUID","null");
        			if (beaconUUID.equals("null")) {beaconUUID = null;}
        			Integer beaconMajor = config.getInt("MAJOR",-1);
        			if (beaconMajor==-1) {beaconMajor = null;}
        			Integer beaconMinor = config.getInt("MINOR",-1);
        			if (beaconMinor==-1) {beaconMinor = null;}
        				try {
        					this.iBeaconManager.startRangingBeaconsInRegion(new Region(beaconRegionId, beaconUUID,beaconMajor, beaconMinor));						
        				} 
        				catch (RemoteException e) {
        				}
        				if(iBeaconManager.rangeRegionAdd){    				
        					if (BleBeaconRuntime.debugRuntime)Log.i(TAG, " Requested region valid and started !");
        					beaconRequest=true;
        					if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Ranging request started for regionId: "+beaconRegionId+" Major: "+beaconMajor+" Minor: "+ beaconMinor);    				
        				}
        				else { 
        					if (BleBeaconRuntime.debugRuntime)Log.i(TAG,"Region already ranging!!");
        					sendContextRequestError(requestId, "Region already ranging!", ErrorCodes.CONFIGURATION_ERROR);
        				}
    			}
        	else if(config.getString("BEACON_OPERATION").equalsIgnoreCase("STOP_RANGING")){
        		    if (BleBeaconRuntime.debugRuntime) Log.w(TAG, "Request to stop ranging received....");	
        			//Log.d(TAG, "Request id " + requestId);	
        			String beaconRegionId = config.getString("REGIONID");
        			String beaconUUID = config.getString("UUID","null");
        			if (beaconUUID.equals("null")) {beaconUUID = null;}
        			Integer beaconMajor = config.getInt("MAJOR",-1);
        			if (beaconMajor==-1) {beaconMajor = null;}
        			Integer beaconMinor = config.getInt("MINOR",-1);
        			if (beaconMinor==-1) {beaconMinor = null;}
        			// this.BEACON_MAC = config.getString("MONITOR_REGION_MAC");
        			Region mRegion = new Region(beaconRegionId, beaconUUID,beaconMajor, beaconMinor);
        			Iterator<Region> regionIterator = iBeaconManager.getRangedRegions().iterator();
        			boolean regionRemoved=false;
        			if (BleBeaconRuntime.debugRuntime) Log.i(TAG,"\nregions ranging = "+ iBeaconManager.getRangedRegions().size());
        			if(iBeaconManager.getRangedRegions()!=null){
        				while(regionIterator.hasNext()){		    		
        					//if(regionIterator.next().equals(mRegion)){
        						try {			    				
        							iBeaconManager.stopRangingBeaconsInRegion(mRegion);	
        						} 
        						catch (RemoteException e) {			    				
        						} 
        						if(iBeaconManager.rangeRegionDelete){
        							if (BleBeaconRuntime.debugRuntime) Log.i(TAG, " Requested ranging stopped !!");	
        							regionRemoved=true;
        							break;
        						}		    			
        					//}
        				}
        			}
		    	if (!regionRemoved){
		    		 //sendContextRequestSuccess(requestId);
		    		if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "STOP_RANGING request does not match any of the current ranged regions or no such regions registered!");
	    			 sendContextRequestError(requestId, "STOP_RANGING request does not match any current ranging regions!", ErrorCodes.CONFIGURATION_ERROR);
		    		}			    	
		    }        
		    else {
		    	  if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Incorrect BEACON_OPERATION requested for ContextType:"+contextType);	
		    	  sendContextRequestError(requestId, "Incorrect BEACON_OPERATION requested", ErrorCodes.CONFIGURATION_NOT_SUPPORTED);		    	  
		    }     
        }    
		else if (contextType.equalsIgnoreCase(MonitorBeaconInfo.CONTEXT_TYPE)) {
				if(config.getString("BEACON_OPERATION").equalsIgnoreCase("MONITOR")){
					Log.d(TAG, "Request to start monitoring received......");
					//this.requestid=requestId;
					//Log.d(TAG, "Request id " + requestId);
					String beaconRegionId = config.getString("REGIONID");
					String beaconUUID = config.getString("UUID","null");
					if (beaconUUID.equals("null")) {beaconUUID = null;}
					Integer beaconMajor = config.getInt("MAJOR",-1);
					if (beaconMajor==-1) {beaconMajor = null;}
					Integer beaconMinor = config.getInt("MINOR",-1);
					if (beaconMinor==-1) {beaconMinor = null;}					
			    	//	if(regionIterator.next().equals(mRegion)){ 
			    			try {
			    				this.iBeaconManager.startMonitoringBeaconsInRegion(new Region(beaconRegionId, beaconUUID,beaconMajor, beaconMinor));						
			    			} 
			    			catch (RemoteException e) {
			    			}
			    			if(iBeaconManager.monitorRegionAdd){ //will it be checked at proper time
			    				//sendContextRequestSuccess(requestId);
			    				if (BleBeaconRuntime.debugRuntime)Log.i(TAG, " Requested motnior valid and started !");
			    				if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Monitoring request started for regionId: "+beaconRegionId+" Major: "+beaconMajor+" Minor: "+ beaconMinor);			    				
			    			}
			    			else {
			    				if (BleBeaconRuntime.debugRuntime)Log.i(TAG,"Region already monitored!!"); 
			    			    sendContextRequestError(requestId, "Region already monitored!", ErrorCodes.CONFIGURATION_ERROR);
			    			}
			    //	}
					//sendContextRequestSuccess(requestId);
					
				}
			    else if(config.getString("BEACON_OPERATION").equalsIgnoreCase("STOP_MONITOR")){
			    	if (BleBeaconRuntime.debugRuntime)Log.d(TAG, "Request to stop monitoring received....");	
			    	//Log.d(TAG, "Request id " + requestId);	
			    	String beaconRegionId = config.getString("REGIONID");
			    	String beaconUUID = config.getString("UUID","null");
			    	if (beaconUUID.equals("null")) {beaconUUID = null;}
			    	Integer beaconMajor = config.getInt("MAJOR",-1);
			    	if (beaconMajor==-1) {beaconMajor = null;}
			    	Integer beaconMinor = config.getInt("MINOR",-1);
			    	if (beaconMinor==-1) {beaconMinor = null;}
			    	// this.BEACON_MAC = config.getString("MONITOR_REGION_MAC");
			    	Region mRegion = new Region(beaconRegionId, beaconUUID,beaconMajor, beaconMinor);					
			    	Iterator<Region> regionIterator = iBeaconManager.getMonitoredRegions().iterator();
			        boolean regionRemoved=false;
			        if(iBeaconManager.getMonitoredRegions()!=null){
			    	while(regionIterator.hasNext()){
			    		//Region monitor = regionIterator.next();
			    		//if(regionIterator.next().equals(mRegion)){ 		    			
			    			try {			    				
			    				 iBeaconManager.stopMonitoringBeaconsInRegion(mRegion);	
			    			} 
			    			catch (RemoteException e) {			    				
			    			}			    			
			    			if(iBeaconManager.monitorRegionDelete){ //will it be checked at proper time??
			    			    //sendContextRequestSuccess(requestId);
			    				if (BleBeaconRuntime.debugRuntime)Log.i(TAG, " Requested motnior stopped !!");	
			    			   regionRemoved=true;
			    			   break;
			    			}
			    			
			    		//}  
			    	}
			        }
			    	if (!regionRemoved){
			    		 //sendContextRequestSuccess(requestId);
			    		if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "STOP_MONITOR request does not match any of the current mornitored regions or no regions registered!");
		    			 sendContextRequestError(requestId, "STOP_MONITOR request does not match any current mornitored regions!", ErrorCodes.CONFIGURATION_ERROR);
			    		}			    	
			    }
			    else {
			    	if (BleBeaconRuntime.debugRuntime)Log.i(TAG, "Incorrect BEACON_OPERATION for contexttype"+ contextType);	
			    	  sendContextRequestError(requestId, "Incorrect BEACON_OPERATION", ErrorCodes.CONFIGURATION_NOT_SUPPORTED);			    	  
			    }
				}
		else {
	    	sendContextRequestError(requestId, "NO_CONTEXT_SUPPORT for "+ contextType, ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
		}		
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

    //@Override
    public void doManualContextScan() {
        // Not supported
    }


}

