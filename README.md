# Welcome
This wiki provides an overview of the `org.ambientdynamix.contextplugins.BleBeacon` plug-in for the [Dynamix Framework](http://ambientdynamix.org). This plug-in finds all active beacons in proximity, find one or a particular set of beacons (Ranging) and receive auto-alert when nearing any beacon or beacons. (Monitoring). Plug-in provides name, distance, RSSI, identifiers etc. of each detected beacon. Plug-in also allows changing scan duration to detect beacons. Plug-in can handle multiple ranging and monitoring requests simultaneously.

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* BLE is available only from Android 4.3 onwards (API level 18), download API level 18 and 19. 
* Dynamix version 2.1 and higher
* Host device Bluetooth hardware
* To build the plug-in using maven, separately install the API’s as maven central repository doesn’t have the latest Android API’s. Follow the instructions of the “Maven-android SDK deployer” given below, to import the libraries from your local installed Android SDK.
(https://github.com/mosabua/maven-android-sdk-deployer)

# Context Support
The plugin provides two types of context.

* `org.ambientdynamix.contextplugins.BleBeacon.beaconRanging` :   Finds all active beacons in proximity and provides basic information of the detected beacons.  The beacon information presently comprises of 10 parameters (name, mac-address, proximity, distance (meters), region-id, uuid, major, minor, broadcasting power, rssi). 
* `org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring`  Receive auto-alert when nearing any beacon or beacons. 
* `org.ambientdynamix.contextplugins.BleBeacon.beaconExtra`  Provides additional informtion about the beacons. Currently NOT supported in android. 

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in.
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.bluetooth.a2dp", "org.ambientdynamix.contextplugins.BleBeacon.beaconRanging", callback, listener);
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.bluetooth.a2dp", "org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring", callback, listener);
```

## Interact with beacons
Once context support is added for `org.ambientdynamix.contextplugins.BleBeacon.beaconRanging` or `org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring`, you can now interact with the beacons through the Dynamix `handleConfiguredContextRequest` method using configuration arguments that specify the command requested, plus extra data in some cases.

Note that the following methods return data using a Dynamix `callback`, as described [here](http://ambientdynamix.org/documentation/integration/).

Presently, plug-in provides 6 operations: CURRENT_BEACONS, RANGE, STOP_RANGING, MONITOR, STOP_MONITORING, SET_FOREGROUND_SCAN_TIMES. Client should send configured request bundle based on operation required. 

###Detect all active beacons  
Specify “CURRENT_BEACONS” as “BEACON_OPERATION” 
```
#!java
Bundle currentBeacons = new Bundle(); 
currentBeacons.putString("BEACON_OPERATION","CURRENT_BEACONS");
```
###Range a particular beacon
Specify  “RANGE” as “BEACON_OPERATION”
```
#!java
Bundle startRangingRegion = new Bundle(); 
startRangingRegion.putString("BEACON_OPERATION","RANGE");
startRangingRegion.putString("REGIONID","FCIbeacons"); //RegionID can never be null. 
//Fields UUID, MAJOR and MINOR are optional and not required if you don’t want to range single or particular set of beacons and simply interested only in ranging.
startRangingRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
startRangingRegion.putInt("MAJOR",25000);
startRangingRegion.putInt("MINOR",10002);
```
###Stop ranging
Specify  “STOP_RANGING” as “BEACON_OPERATION”
```
#!java
Bundle stopRanging1 = new Bundle(); 
stopRanging1.putString("BEACON_OPERATION","STOP_RANGING");
stopRanging1.putString("REGIONID"," currentBeacons"); // always provide the exact region identifiers used when started ranging
```
###Monitor a region
Specify  “MONITOR” as “BEACON_OPERATION”
```
#!java
Bundle startMonitorRegion = new Bundle(); {
startMonitorRegion.putString("BEACON_OPERATION","MONITOR");
startMonitorRegion.putString("REGIONID","WorkDesk");
startMonitorRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
startMonitorRegion.putInt("MAJOR",25000);
startMonitorRegion.putInt("MINOR",10004);
```

###Stop Monitoring a region
Specify  “STOP_MONITOR” as “BEACON_OPERATION”
```
#!java
Bundle stopMonitorRegion1 = new Bundle(); 
stopMonitorRegion1.putString("BEACON_OPERATION","STOP_MONITOR");
stopMonitorRegion1.putString("REGIONID","rid"); // always provide the exact region identifiers used when started monitoring.
```

###To set scan time
Specify  “SET_FOREGROUND_SCAN_TIMES” as “BEACON_OPERATION” 
```
#!java
Bundle scantimes = new Bundle(); 
scantimes.putString("BEACON_OPERATION","SET_FOREGROUND_SCAN_TIMES");
scantimes.putLong("FOREGROUND_SCAN_PERIOD",5001); //Time in milliseconds.
scantimes.putLong("FOREGROUND_BETWEEN_SCAN_PERIOD",0);
```
## Handle Events
Once context support is added for the context types, the supplied listener will start receiving events based on configuration bundle that was sent along. Events implement the Dynamix `IContextInfo` interface, which provides a variety of general event metadata (e.g., the event time, source and expiry information). 
 
1. When context support for `org.ambientdynamix.contextplugins.BleBeacon.beaconRanging` is added, the listner will receive `IBeaconArray` events, which includes the total number of detected beacons and beacon information:
    * `ArrayList<BeaconInfo> getCurrentBeacons()` : A list of beacons and their information.
    * `BeaconInfo` : includes seperat getter methods for the 10 parameters mentioned above
        * `String getName()` : Presently, name of Estimote beacons is preset to “Estimote” and cannot be modified
        * `String getMacAddress()` : Each beacon (each Ble device) has a unique mac –address.
        * `int getProximity()` : Proximity is divided in 4 proximate regions based on distance and every region is assigned a number. 0- Unknown(Unknown); 1- Immediate (Less than 0.5 meters); 2- Near (Less than 4 meters); 3- Far (Greater than 4 meters)
        * `double getDistance()` :
        * `String getRegionID()` : : Region identifier. Name (String) of the region/location of the beacon. It could be location name, name of product category in a shop or any relevant name of what the beacon intends to advertise
        * `String getProximityUUID()` : Manufacturer identifier. It is 16 bytes. 
        * `int getMajor()` : Major type of identifier. It is 2 bytes. E.g. If regionID is “Shoes”, then major identifier of a beacon can represent a category of shoes like“Sport” or “Formal” in a shop. 
        * `int getMinor()` : Minor type of identifier. It is 2 bytes. E.g. If regionID is “Shoes”, major represents category “Sport”, than minor identifier of beacon can represent further category of Sport shoes like “ Running” or “Tennis” or simply represent a single shoe. 
        * `int getBroadcastingPower()` :
        * `int getRssi()`
        * Region-id, UUID, Major and Minor together represent a unique beacon.

2. When context support for `org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring` is added, the listner will receive `IMonitorBeaconInfo` events, which has the following addtional data: 
    * `int getNewMonitorState()` : tells whether phone/person has entered (1) or exited (0) from the monitored beacon region
    * `String getuniqueId():`
    * `String getProximityUUID():`
    * `Integer getMajor():`
    * `Integer getMinor():`	

# Web App Usage
Integrate the Dynamix Framework within your web app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your web application, see [this guide](http://ambientdynamix.org/documentation/web-app-quickstart).

## Add context support for the plug-in.
TODO

## Handle Events
TODO