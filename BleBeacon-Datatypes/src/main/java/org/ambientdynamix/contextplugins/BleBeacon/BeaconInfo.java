/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.BleBeacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class BeaconInfo implements IBeaconInfo {
	
	private final String TAG = ((Object)this).getClass().getSimpleName();
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.BleBeacon.beaconinfo";	
	public ArrayList<BeaconInfo> newIbeacons = new ArrayList<BeaconInfo>();	
		
	// Single Beacon Info 10 parameters
	private String name;
	private String macaddress;
	private int proximity;
	private double distance;
	private String regionID;
	private String proximityUUID;
	private int major;
	private int minor;
	private int broadcastingpower;	
	private int rssi;
	
		
	// Getters for Beacon 10 parameters
	@Override	
	public String getName() {
			   return name;
	}	
	
	@Override
	public String getMacAddress() {
	   return macaddress;
	}
	
	@Override
	public int getProximity() {
	   return proximity;
	}
	
    @Override
	public double getDistance() {
	   return distance;
	}	
    
    @Override
	public String getRegionID() {
		return regionID;
	}
	@Override
	public String getProximityUUID() {
		return proximityUUID;
	}

	@Override
	public int getMajor() {
		return major;
	}

	@Override
	public int getMinor() {
		return minor;
	}	
	
	
	@Override
	public int getBroadcastingPower() {
		return broadcastingpower;
	}

	@Override
	public int getRssi() {
		return rssi;
	}	
	
	// Standard getter methods of IContextInfo
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}
	
	@Override
	public String getImplementingClassname() {
		return ((Object)this).getClass().getName();
	}

	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();// So there is no duplication of formats
		formats.add("text/plain");
		return formats;
	}

	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "Beacon minor =" + minor;
		else
			return "";
	}
   
		protected BeaconInfo(String name, String macaddress, int proximity, double distance, String regionId,String proximityUUID, int major, int minor, int broadcastingpower, int rssi) {
        	Log.d(TAG, "BeaconInfo called\n");		
			this.name = name;
			this.macaddress = macaddress;
			this.proximity = proximity;
			this.distance = distance;
			this.regionID=regionId;
			this.proximityUUID = proximityUUID;
			this.major = major;
			this.minor = minor;
			this.broadcastingpower = broadcastingpower;
			this.rssi = rssi;			
	}		

	
	
	@Override
	public String toString() {
		return ((Object)this).getClass().getSimpleName();
	}

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
		out.writeString(macaddress);	
		out.writeInt(proximity);
		out.writeDouble(distance);
		out.writeString(regionID);
		out.writeString(proximityUUID);
		out.writeInt(major);
	    out.writeInt(minor);
		out.writeInt(broadcastingpower);
		out.writeInt(rssi);	
	}
	
	public static Parcelable.Creator<BeaconInfo> CREATOR = new Parcelable.Creator<BeaconInfo>() {

		public BeaconInfo createFromParcel(Parcel in) {
			return new BeaconInfo(in);
		}

		public BeaconInfo[] newArray(int size) {
			return new BeaconInfo[size];
		}
	};
	

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data
	 * sent over IPC.
	 */
	
	private BeaconInfo(final Parcel in) {				      
		name=in.readString();
		macaddress=in.readString();
		proximity=in.readInt();
		distance=in.readDouble();
		regionID=in.readString();
		proximityUUID=in.readString();
		major=in.readInt();
		minor=in.readInt();		
		broadcastingpower=in.readInt();
		rssi=in.readInt();		
	}	
		
	@Override
	public int describeContents() {
		return 0;
	}
	
	
}