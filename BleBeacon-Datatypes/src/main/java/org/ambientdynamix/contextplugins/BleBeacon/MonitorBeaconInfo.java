package org.ambientdynamix.contextplugins.BleBeacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;


public class MonitorBeaconInfo implements IMonitorBeaconInfo {
	
	public static Parcelable.Creator<MonitorBeaconInfo> CREATOR = new Parcelable.Creator<MonitorBeaconInfo>() {

		public MonitorBeaconInfo createFromParcel(Parcel in) {
			return new MonitorBeaconInfo(in);
		}

		public MonitorBeaconInfo[] newArray(int size) {
			return new MonitorBeaconInfo[size];
		}
	};
	
	private final String TAG = ((Object)this).getClass().getSimpleName();
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring";
	private int newMonitorState;
	private String uniqueId;
	private String proximityUUID;
	private Integer major;
	private Integer minor;
	private static boolean debugMBI = false;

    public static void setDebug(boolean debug) {
    	MonitorBeaconInfo.debugMBI = debug;
    }
		
	@Override
	public int getNewMonitorState() {
		
		return newMonitorState;
	}	
	
	@Override
	public String getuniqueId() {
		return uniqueId;
	}
	
	@Override
	public String getProximityUUID() {
		return proximityUUID;
	}

	@Override
	public Integer getMajor() {
		return major;
	}

	@Override
	public Integer getMinor() {
		return minor;
	}	
	
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
		
	}
	
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "Notification = "+ newMonitorState + " for RegionID:" + uniqueId + " Major:" + major + " Minor:" + minor;
		else
			return "";
	}

	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}

	@Override
	public String getImplementingClassname() {		
		return ((Object)this).getClass().getName();
	}
	

	
	public MonitorBeaconInfo(int state, String UniqueId,String ProximityUuid, Integer Major, Integer Minor){
		if (MonitorBeaconInfo.debugMBI)Log.d(TAG, "In MonitorBeaconInfo Constr.");
		newMonitorState=state;
		uniqueId=UniqueId;
		proximityUUID=ProximityUuid;
		major=Major;
		minor=Minor;
		
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {		
		out.writeInt(newMonitorState);
		out.writeString(uniqueId);
		out.writeString(proximityUUID);
		out.writeValue(major);
		out.writeValue(minor);
		}
	
	
		
	private MonitorBeaconInfo(final Parcel in) {
		newMonitorState=in.readInt();
		uniqueId=in.readString();
		proximityUUID=in.readString();
		major=(Integer) in.readValue(getClass().getClassLoader());
		minor=(Integer) in.readValue(getClass().getClassLoader());			
	}	
	


	@Override
	public int describeContents() {		
		return 0;
	}	

	

}
