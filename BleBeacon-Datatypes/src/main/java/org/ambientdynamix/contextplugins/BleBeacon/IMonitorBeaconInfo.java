package org.ambientdynamix.contextplugins.BleBeacon;

import org.ambientdynamix.api.application.IContextInfo;

public interface IMonitorBeaconInfo extends IContextInfo {
	
	public int getNewMonitorState();	
	public String getuniqueId();	
	public String getProximityUUID();
	public Integer getMajor();
	public Integer getMinor();	

}
