/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.BleBeacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class BeaconArray implements IBeaconArray {
	
	private final String TAG = ((Object)this).getClass().getSimpleName();
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.BleBeacon.beaconRanging";	
	public ArrayList<BeaconInfo> currentBeacons = new ArrayList<BeaconInfo>();	
	private static boolean debugBeaconArray = true;

	    public static void setDebug(boolean debug) {
	    	BeaconArray.debugBeaconArray = debug;
	    }
	
	@Override
	public ArrayList<BeaconInfo> getCurrentBeacons() {
		return currentBeacons;
	}	
	// Standard IContextInfo getter methods
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}
	
	@Override
	public String getImplementingClassname() {
		return ((Object)this).getClass().getName();
	}

	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();// So there is no duplication of formats
		formats.add("text/plain");
		return formats;
	}

	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "No of Beacons = " + currentBeacons.size();
		else
			return "";
	}
   
   
	public BeaconArray(ArrayList<BeaconInfo> newBeacons) {		
		currentBeacons = newBeacons;
		if (BeaconArray.debugBeaconArray) Log.d(TAG, "Received beacon array");		
		}	

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeList(currentBeacons);
		
	}	
	
	public static Parcelable.Creator<BeaconArray> CREATOR = new Parcelable.Creator<BeaconArray>() {

		public BeaconArray createFromParcel(Parcel in) {
			return new BeaconArray(in);
		}

		public BeaconArray[] newArray(int size) {
			return new BeaconArray[size];
		}
	};	

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data
	 * sent over IPC.
	 */
	
	private BeaconArray(final Parcel in) {	
		in.readList(currentBeacons, getClass().getClassLoader());
	}
	
	@Override
	public String toString() {
		return ((Object)this).getClass().getSimpleName();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
}