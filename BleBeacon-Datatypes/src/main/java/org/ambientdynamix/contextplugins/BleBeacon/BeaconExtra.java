/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.BleBeacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class BeaconExtra implements IBeaconExtra {
	
	private final String TAG = ((Object)this).getClass().getSimpleName();
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.BleBeacon.beaconExtra";	
	private int batteryLevel;
    private int advertisingInterval;	
	private String operatingSystem;
	private String hardwareRevision;
	
	@Override
	public int getBatteryLevel() {
		return batteryLevel;
	}
	
	@Override
	public int getAdvertisingInterval() {
		return advertisingInterval;
	}    
	
	@Override
	public String getOperatingSystem() {
		return operatingSystem;
	}

	@Override
	public String getHardwareRevision() {
		return hardwareRevision;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Standard IContextInfo getter methods
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}
	
	@Override
	public String getImplementingClassname() {
		return ((Object)this).getClass().getName();
	}

	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();// So there is no duplication of formats
		formats.add("text/plain");
		return formats;
	}

	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "";
		else
			return "";
	}
   
   
	

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	@Override
	public void writeToParcel(Parcel out, int flags) {
		
		
	}	
	
	public static Parcelable.Creator<BeaconExtra> CREATOR = new Parcelable.Creator<BeaconExtra>() {

		public BeaconExtra createFromParcel(Parcel in) {
			return new BeaconExtra(in);
		}

		public BeaconExtra[] newArray(int size) {
			return new BeaconExtra[size];
		}
	};	

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data
	 * sent over IPC.
	 */
	
	private BeaconExtra(final Parcel in) {	
		
	}
	
	@Override
	public String toString() {
		return ((Object)this).getClass().getSimpleName();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
}