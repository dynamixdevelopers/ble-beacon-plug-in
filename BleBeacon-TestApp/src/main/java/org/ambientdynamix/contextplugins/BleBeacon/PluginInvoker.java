package org.ambientdynamix.contextplugins.BleBeacon;

import android.os.Bundle;
import android.util.Log;

import org.ambientdynamix.api.application.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class PluginInvoker {


    private List<PluginInvocation> pluginInvocations;  
    private List<ContextType> contextTypes; // For list of all context types.

    /**
     * if this is set to true the BindDynamixActivity will not require any user interaction to run
     */
    public static final boolean AUTOMATIC_EXECUTION = false;

    private String TAG = this.getClass().getSimpleName();
    public static boolean debugPI = false;

    private static void setDebug(boolean debug) {
    	PluginInvoker.debugPI = debug;
    }
    
    public List<PluginInvocation> getPluginInvocations() {
        return pluginInvocations;
    }
    
    public List<ContextType> getContextTypes() {
        return contextTypes;
    }

    
    //Example Bundle to request all beacons in proximity
    Bundle currentBeacons = new Bundle(); {
    	currentBeacons.putString("BEACON_OPERATION","CURRENT_BEACONS");
    }
    
    // Example Bundle to monitor. Fields UUID, MAJOR and MINOR are optional.
    Bundle startMonitorRegion = new Bundle(); {
    	startMonitorRegion.putString("BEACON_OPERATION","MONITOR");
    	startMonitorRegion.putString("REGIONID","WorkDesk");
    	startMonitorRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	startMonitorRegion.putInt("MAJOR",25000);
    	startMonitorRegion.putInt("MINOR",10004);
        //monitorRegion.putString("MONITOR_REGION_MAC","CC:43:46:70:5A:12"); // most defining identifier, only this can suffice too. 
    	}
    
    // Example Bundle to stop monitor. Enter only those field values used during start monitor.
    Bundle stopMonitorRegion = new Bundle();{
		stopMonitorRegion.putString("BEACON_OPERATION", "STOP_MONITOR");
		stopMonitorRegion.putString("REGIONID", "WorkDesk");
		stopMonitorRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
		stopMonitorRegion.putInt("MAJOR", 25000);
		stopMonitorRegion.putInt("MINOR", 10004);
	}
    
    Bundle startMonitorRegion1 = new Bundle(); {
    	startMonitorRegion1.putString("BEACON_OPERATION","MONITOR");
    	startMonitorRegion1.putString("REGIONID","rid");
    	//startMonitorRegion1.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	//startMonitorRegion1.putInt("MAJOR",25000);
    	//startMonitorRegion1.putInt("MINOR",10004);
    }    
    
    Bundle stopMonitorRegion1 = new Bundle(); {
    	stopMonitorRegion1.putString("BEACON_OPERATION","STOP_MONITOR");
    	stopMonitorRegion1.putString("REGIONID","rid");
    	//stopMonitorRegion1.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	//stopMonitorRegion1.putInt("MAJOR",25000);
    	//stopMonitorRegion1.putInt("MINOR",10004);
    }  	
	
    // Example Bundle to range. Fields UUID, MAJOR and MINOR are optional.
	Bundle startRangingRegion = new Bundle(); {
		startRangingRegion.putString("BEACON_OPERATION","RANGE");
		startRangingRegion.putString("REGIONID","FCIbeaconTest");
		startRangingRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
		startRangingRegion.putInt("MAJOR",25000);
		startRangingRegion.putInt("MINOR",10002);
   }
	// Example Bundle to stop range. Enter only those field values used during start range.
    Bundle stopRangingRegion = new Bundle(); {
    	stopRangingRegion.putString("BEACON_OPERATION","STOP_RANGING");
    	stopRangingRegion.putString("REGIONID","FCIbeaconTest");
    	stopRangingRegion.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	stopRangingRegion.putInt("MAJOR",25000);
    	stopRangingRegion.putInt("MINOR",10002);
   }
    
    Bundle startRanging1 = new Bundle(); {
    	startRanging1.putString("BEACON_OPERATION","RANGE");
    	startRanging1.putString("REGIONID","rid");
    	startRanging1.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	startRanging1.putInt("MAJOR",25000);
    	startRanging1.putInt("MINOR",10002);
   }
    
    Bundle stopRanging1 = new Bundle(); {
    	stopRanging1.putString("BEACON_OPERATION","STOP_RANGING");
    	stopRanging1.putString("REGIONID"," currentBeacons");
    	//stopRanging1.putString("UUID","B9407F30-F5F8-466E-AFF9-25556B57FE6D");
    	//stopRanging1.putInt("MAJOR",25000);
    	//stopRanging1.putInt("MINOR",10002);
   }
   // Example Bundle to set foreground scan time. Time in milliseconds.
   Bundle scantimes = new Bundle(); {
    	scantimes.putString("BEACON_OPERATION","SET_FOREGROUND_SCAN_TIMES");
    	scantimes.putLong("FOREGROUND_SCAN_PERIOD",5001);
    	scantimes.putLong("FOREGROUND_BETWEEN_SCAN_PERIOD",0);
   }
    /**
     * constructor of the PluginInvoker. Add plugin Invocations here
     */
    public PluginInvoker() {
        pluginInvocations = new Vector<PluginInvocation>();    
        contextTypes = new Vector<ContextType>();
        addContextType("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconRanging");
        addContextType("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring");
        // Add plugin invocations by calling addPluginInvocation(pluginId,context type, configuration)        
        
        addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconRanging",currentBeacons);
        addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconRanging",stopRanging1);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconRanging",startRangingRegion);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconRanging",stopRangingRegion);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring",startMonitorRegion);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring",stopMonitorRegion);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring",startMonitorRegion1);
        //addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconMonitoring",stopMonitorRegion1);
       
      
        if (PluginInvoker.debugPI)Log.d(TAG, "Plugin inovcation added ");
    }
    
    
    private void addContextType(String pluginId, String contextType){
    	contextTypes.add(new ContextType(pluginId, contextType));
       	
    }
    
    public void setScanTime() {
		 addPluginInvocation("org.ambientdynamix.contextplugins.BleBeacon","org.ambientdynamix.contextplugins.BleBeacon.beaconranging",scantimes);		
	}
    /**
    *
    * @param pluginId    The plugin to call
    * @param contextType  The contextType  to request
    * @param configuration  optional configuration bundle, can be null
    */
   private void addPluginInvocation(String pluginId, String contextType, Bundle configuration) {
       pluginInvocations.add(new PluginInvocation(pluginId,contextType,configuration));
   }
   
   
    /**
     * This gets called when the DynamixBindActivity receives a context event of a type specified in the cotextRequestType array
     *
     * @param event
     */
    public void invokeOnResponse(ContextResult event) {

        // Check for native IContextInfo
        if (event.hasIContextInfo()) {
        	if (PluginInvoker.debugPI) Log.i(TAG, "A1 - Event contains native IContextInfo: " + event.getIContextInfo());
            IContextInfo nativeInfo = event.getIContextInfo();
                
            if (PluginInvoker.debugPI) Log.i(TAG, "A1 - IContextInfo implementation class: " + nativeInfo.getImplementingClassname());
            

            if (nativeInfo instanceof IBeaconArray){            	
            	IBeaconArray info = (IBeaconArray) nativeInfo;
                ArrayList<BeaconInfo> currentBeacons = info.getCurrentBeacons();
                if(currentBeacons.size()>0){             
                	Log.i(TAG,"--------------------Beacons Detected--------------------------");                	          
                	for (BeaconInfo newBeacon: currentBeacons) {                		                 
                		Log.i(TAG, newBeacon.getName()+" Beacon at "+newBeacon.getDistance()+" meters (RegionId:" + newBeacon.getRegionID()+ " Major:"+newBeacon.getMajor() + " Minor:"+newBeacon.getMinor()+")");                
                		}   
                	currentBeacons.clear();
                }
                else { 
                	Log.i(TAG,"---- NO BEACONS DETECTED----");
                }
            }            
            else if(nativeInfo instanceof IMonitorBeaconInfo){
            	IMonitorBeaconInfo info = (IMonitorBeaconInfo) nativeInfo;
            	Log.i(TAG,"------------------Auto-Notification--------------------------");
            	Log.i(TAG,"state ="+ info.getNewMonitorState()+" (1-Inside/0-Outside) (Regionid:" + info.getuniqueId()+
            			" Major:" + info.getMajor()+" Minor:" + info.getMinor()+")");            	
            }        
        } else
            Log.i(TAG, "Event does NOT contain native IContextInfo... we need to rely on the string representation!");
    }

    

    public class PluginInvocation {
        private String pluginId;
        private String contextRequestType;
        private Bundle configuration;
        private boolean successfullyCalled;

        public PluginInvocation(String pluginId, String contextRequestType, Bundle configuration) {
            this.pluginId = pluginId;
            this.contextRequestType = contextRequestType;
            this.configuration = configuration;
        }

        public String getPluginId() {
            return pluginId;
        }

        public void setPluginId(String pluginId) {
            this.pluginId = pluginId;
        }

        public String getContextRequestType() {
            return contextRequestType;
        }

        public void setContextRequestType(String contextRequestType) {
            this.contextRequestType = contextRequestType;
        }

        public Bundle getConfiguration() {
            return configuration;
        }

        public void setConfiguration(Bundle configuration) {
            this.configuration = configuration;
        }

        public boolean isSuccessfullyCalled() {
            return successfullyCalled;
        }

        public void setSuccessfullyCalled(boolean successfullyCalled) {
            this.successfullyCalled = successfullyCalled;
        }
    }
    
    
    
    public class ContextType {
    	private String pluginId;
        private String contextType;
        private boolean successfullyCalled;
        
        public ContextType(String pluginId, String type) {
            this.pluginId = pluginId;
            this.contextType = type;            
        }
        public String getPluginId() {
            return pluginId;
        }

        public void setPluginId(String pluginId) {
            this.pluginId = pluginId;
        }

        public String getContextType() {
            return contextType;
        }

        public void setContextRequestType(String type) {
            this.contextType = type;
        }
        
        public boolean isSuccessfullyCalled() {
            return successfullyCalled;
        }

        public void setSuccessfullyCalled(boolean successfullyCalled) {
            this.successfullyCalled = successfullyCalled;
        }
    	
    }



	
    

}